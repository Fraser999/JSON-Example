extern crate rustc_serialize;

use rustc_serialize::json;
use rustc_serialize::Encodable;

// Automatically generate `RustcDecodable` and `RustcEncodable` trait
// implementations
#[derive(RustcDecodable, RustcEncodable)]
pub struct TestStruct {
    data_int: u8,
    data_str: String,
    data_vector: Vec<u8>,
}

fn main() {
    let object = TestStruct {
        data_int: 1,
        data_str: "homura".to_string(),
        data_vector: vec![2,3,4,5],
    };

    // Serialize using `json::encode`
    let encoded = json::encode(&object).unwrap();

    let mut pretty_encoded = String::new();
    {
        let mut pretty_encoder = json::Encoder::new_pretty(&mut pretty_encoded);
        object.encode(&mut pretty_encoder).unwrap();
    }

    let pretty_str = format!("{}", json::as_pretty_json(&object));

    // Deserialize using `json::decode`
    let _decoded: TestStruct = json::decode(&encoded).unwrap();

    println!("1: {}", encoded);
    println!("2: {}", pretty_encoded);
    println!("3: {}", json::as_json(&object));
    println!("4: {}", json::as_pretty_json(&object));
    println!("5: {}", pretty_str);
}
